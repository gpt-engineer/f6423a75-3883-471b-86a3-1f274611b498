document
  .getElementById("waitlist-form")
  .addEventListener("submit", function (event) {
    event.preventDefault();
    var email = document.getElementById("email").value;
    if (email) {
      document.getElementById("success-message").hidden = false;
      event.target
        .querySelector("button")
        .classList.replace("bg-red-400", "bg-green-400");
    }
  });
